using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Smartphone : MonoBehaviour
{
	public enum Mode
	{
		Off,
		Standby,
		Text,
		NFCScanning
	}

	private Mode mCurrentMode = Mode.Off;

	private Dictionary<Mode, SmartphoneScreen> mScreens = new Dictionary<Mode, SmartphoneScreen>();

	public SmartphoneScreen GetSmartphoneScreen(Mode mode) { return mScreens[mode]; }

	private void Awake()
	{
		SmartphoneScreen[] screens = GetComponentsInChildren<SmartphoneScreen>();
		SmartphoneScreen screen;

		for (int i = 0; i < screens.Length; ++i)
		{
			screen = screens[i];

			mScreens.Add(screen.Mode, screen);
		}
	}

	public void ChangeMode(Mode mode)
	{
		mScreens[mCurrentMode].gameObject.SetActive(false);

		mCurrentMode = mode;

		mScreens[mCurrentMode].gameObject.SetActive(true);
	}
}
