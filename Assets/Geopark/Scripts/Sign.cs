using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Sign : MonoBehaviour
{
	[SerializeField]
	private string mText;
	public	string Text { get { return mText; } }

	[SerializeField]
	private Text mUIText;

	private void OnValidate()
	{
		if(mUIText)
		{
			mUIText.text = mText;
		}
	}

	// Start is called before the first frame update
	void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
