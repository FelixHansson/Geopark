using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHUDHandler : MonoBehaviour
{
	[SerializeField]
	private GameObject mPlayerHUDGO;

    // Start is called before the first frame update
    void Start()
    {
		mPlayerHUDGO.SetActive(false);
	}

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyUp(KeyCode.Tab))
		{
			mPlayerHUDGO.SetActive(!mPlayerHUDGO.activeSelf);
		}
    }
}
