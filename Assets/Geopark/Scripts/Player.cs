using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
	public enum SmartphonePosition
	{
		Hidden = 0,
		Focus,
		Scanning,
		Sideview
	}

	[SerializeField]
	private GameObject mInGameMenuPanel;

	[SerializeField]
	private Smartphone mSmartphone;

	[SerializeField]
	private SmartphonePosition mSmartphonePosition = SmartphonePosition.Hidden;

	[SerializeField]
	private Transform[] mSmartphonePositions;

	private bool		mUpdateSmartphoneTransform = false;

	private Transform	mSmartphoneTransformTarget;

	private float		mSmartphoneMovementSpeed = 0.5f;
	private float		mSmartphoneRotationSpeed = 180.0f;

	private void Start()
	{
		mInGameMenuPanel.SetActive(false);

		Transform trans = mSmartphonePositions[(int)mSmartphonePosition];

		Vector3		pos = trans.position;
		Quaternion	rot = trans.rotation;

		mSmartphone.transform.SetPositionAndRotation(pos, rot);
	}

	private void Update()
	{
		if(Input.GetKeyUp(KeyCode.Escape))
		{
			mInGameMenuPanel.SetActive(!mInGameMenuPanel.activeSelf);
		}

		if(mInGameMenuPanel.activeSelf)
		{
			return;
		}

		CheckInput();

		if(mUpdateSmartphoneTransform)
		{
			UpdateSmartphoneTransform();
		}
	}

	private void CheckInput()
	{
		if(Input.GetKeyUp(KeyCode.Alpha1))
		{
			ChangeSmartphonePosition(SmartphonePosition.Hidden);
		}
		else if (Input.GetKeyUp(KeyCode.Alpha2))
		{
			ChangeSmartphonePosition(SmartphonePosition.Focus);
		}
		else if (Input.GetKeyUp(KeyCode.Alpha3))
		{
			ChangeSmartphonePosition(SmartphonePosition.Scanning);
		}
		else if (Input.GetKeyUp(KeyCode.Alpha4))
		{
			ChangeSmartphonePosition(SmartphonePosition.Sideview);
		}
	}

	public void ChangeSmartphonePosition(SmartphonePosition smartphonePosition)
	{
		if(mSmartphonePosition == smartphonePosition)
		{
			return;
		}

		mSmartphonePosition = smartphonePosition;

		mSmartphoneTransformTarget = mSmartphonePositions[(int)mSmartphonePosition];

		mUpdateSmartphoneTransform = true;
	}

	private void UpdateSmartphoneTransform()
	{
		Transform currentTransform = mSmartphone.transform;

		Vector3		position = Vector3.MoveTowards(currentTransform.position, mSmartphoneTransformTarget.position, mSmartphoneMovementSpeed * Time.deltaTime);
		Quaternion	rotation = Quaternion.RotateTowards(currentTransform.rotation, mSmartphoneTransformTarget.rotation, mSmartphoneRotationSpeed * Time.deltaTime);

		mSmartphone.transform.SetPositionAndRotation(position, rotation);

		if(mSmartphone.transform.position == mSmartphoneTransformTarget.position)
		{
			if (mSmartphone.transform.rotation == mSmartphoneTransformTarget.rotation)
			{
				mUpdateSmartphoneTransform = false;
			}
		}
	}
}