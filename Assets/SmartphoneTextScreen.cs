using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SmartphoneTextScreen : SmartphoneScreen
{
	[SerializeField]
	private Text mUIText;

	public void SetText(string text)
	{
		mUIText.text = text;
	}
}